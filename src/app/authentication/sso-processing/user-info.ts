import { TokenInfo } from './token-info';
export class UserInfo {
    userName: string;
    loweredUsername: string;
    mobileAlias: string;
    isAnonymous: number;
    userType: number;
    accessTokenInfo: TokenInfo;
    rights: any;
}