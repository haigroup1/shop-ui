import { Router, ActivatedRoute, Params } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../guard/authentication.service';
import { Constants } from '../../am/common/util/constants';
import { Console } from '@angular/core/src/console';
import { TokenInfo } from './token-info';
import { UserInfo } from './user-info';


@Component({
  selector: 'app-sso-processing',
  templateUrl: './sso-processing.component.html',
  styleUrls: ['./sso-processing.component.css'],
  providers: [AuthenticationService]
})
export class SsoProcessingComponent implements OnInit {

  private tokenInfo: TokenInfo;
  private userInfo: UserInfo;
  returnUrl: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private activatedRoute: ActivatedRoute, private authenticationService: AuthenticationService
  ) { }

  ngOnInit() {
    debugger
    this.returnUrl = localStorage.getItem(Constants.RETURN_URL);
    var code = this.getAuthorizationCode()
    // this.tthcService.getTokenId(code)
    //     .then(response => {
    //         debugger
          
    //     })
    //     .catch(error => {
            
    //     });

    this.authenticationService.getUserInfo(code)
      .then(response => {
        debugger
        
        this.userInfo = response.data;
        // this.tokenInfo = this.userInfo.accessTokenInfo;

        let now = new Date().getTime();
        let expireTime = now + (+response.data.accessTokenInfo.expiresIn) * 1000;
        localStorage.setItem('idToken', response.data.accessTokenInfo.idToken)
        localStorage.setItem(Constants.ACCESS_TOKEN, "Bearer " + response.data.accessTokenInfo.accessToken);
        localStorage.setItem(Constants.EXPIRE_TIME, expireTime + "");
        localStorage.setItem(Constants.REFRESH_TOKEN, response.data.accessTokenInfo.refreshToken);
        localStorage.setItem(Constants.IS_AUTHENTIC, "true");
        localStorage.setItem(Constants.CURRENT_USER, JSON.stringify(response.data));

        this.router.navigate([this.returnUrl]);
      })
      .catch(error => {
        localStorage.setItem(Constants.IS_AUTHENTIC, "false");
        // this.router.navigate(["/"]);
      });
  }

  getAuthorizationCode(): string {
    debugger
    let code = this.activatedRoute.snapshot.queryParams["code"];
    localStorage.setItem('code', code);
    return code;
  }

}
