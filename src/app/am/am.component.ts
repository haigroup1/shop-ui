import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication/guard/authentication.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { LanguageItem, LanguageItemList } from '../i18n-setting';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { Http } from '@angular/http';
import { Constants } from './common/util/constants';
import { Observable } from 'rxjs';
import { TokenInfo } from '../authentication/sso-processing/token-info';
import { UserInfo } from '../authentication/sso-processing/user-info';
import { AppConfig } from '../app.config';
@Component({
  selector: 'app-am',
  templateUrl: './am.component.html',
  styleUrls: ['./am.component.css'],
  providers: [Idle]
})
export class AmComponent implements OnInit {

  userInfo: UserInfo;
  tokenInfo: TokenInfo;

  idleState = 'Not started.';
  timedOut = false;
  lastPing?: Date = null;

  private expireTime: number = 0;
  private timer;
  private sub;

  ListLanguage: LanguageItem[];
  SelectedLanguage: LanguageItem;
  constructor(
    private http: Http,
    private idle: Idle,
    private authenticationService: AuthenticationService,
    private router: Router,
    private translate: TranslateService,
  ) {
    this.ListLanguage = LanguageItemList;
    translate.use(localStorage.getItem(Constants.KEY_LANGUAGE));
    // Get current language
    this.ListLanguage.forEach(lang => {
      if (lang.Key === translate.currentLang) {
        this.SelectedLanguage = lang;
      }
    });

    idle.setIdle(5);
    idle.setTimeout(10);
    idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);

    idle.onTimeoutWarning.subscribe((countdown) => this.idleState = 'You will time out in ' + countdown + ' seconds!');


    idle.onTimeout.subscribe(() => {
      alert('Timeout');
      this.idleState = 'Timed out!';
      this.timedOut = true;
      localStorage.clear();
      this.router.navigate(['/auth', { sessionExpirate: 'true' }]);
    });
  }

  ngOnInit() {
    this.userInfo = JSON.parse(localStorage.getItem(Constants.CURRENT_USER));
    console.log("expire time: " + Number(localStorage.getItem(Constants.EXPIRE_TIME)));

    this.countdownTimer();
  }

  logout() {
    // this.tthcService.getTokenId()
    //   .then(response => {
    //       debugger
    //       localStorage.removeItem(Constants.IS_AUTHENTIC);
    //       localStorage.removeItem(Constants.ACCESS_TOKEN);
    //       localStorage.removeItem(Constants.REFRESH_TOKEN);
    //       localStorage.removeItem(Constants.SESSION_STATE);
    //       localStorage.removeItem(Constants.CURRENT_USER);
      
    //       // localStorage.clear()
    //       window.location.href = AppConfig.settings.SSO_LOGOUT_URL;
    //   })
    //   .catch(error => {
          
    //   });
    debugger
    // localStorage.removeItem(Constants.IS_AUTHENTIC);
    // localStorage.removeItem(Constants.ACCESS_TOKEN);
    // localStorage.removeItem(Constants.REFRESH_TOKEN);
    // localStorage.removeItem(Constants.SESSION_STATE);
    // localStorage.removeItem(Constants.CURRENT_USER);
    // '&state=state_1&post_logout_redirect_uri=http://192.168.11.196:9092/processing/'

    var path = AppConfig.settings.SSO_LOGOUT_URL+'?id_token_hint='+localStorage.getItem('idToken')
              + AppConfig.settings.SSO_LOGOUT_QUERY_URL
    /*
      path = https://<WSO2IS_IP>:9443/oidc/logout?
      id_token_hint=eyJ4NXQiOiJOVEF4Wm1NeE5ETXlaRGczTVRVMVpHTTBNekV6T0RKaFpXSTRORE5sWkRVMU9HRmtOakZpTVEiLCJraWQiOiJOVEF4Wm1Ne
      E5ETXlaRGczTVRVMVpHTTBNekV6T0RKaFpXSTRORE5sWkRVMU9HRmtOakZpTVEiLCJhbGciOiJSUzI1NiJ9.eyJhdF9oYXNoIjoiLWdlVHU0X1RoVXFnVDU3
      QlVzTUxMdyIsImF1ZCI6ImdBM2dEOTI2dmxwQjhWcmVfZGxOalRtS3pMY2EiLCJjX2hhc2giOiJDczRmYjBZcFFZcUV1dlUwSVVXMWN3Iiwic3ViIjoiYWRt
      aW4iLCJuYmYiOjE1NjU2Njk0MDIsImF6cCI6ImdBM2dEOTI2dmxwQjhWcmVfZGxOalRtS3pMY2EiLCJhbXIiOlsiQmFzaWNBdXRoZW50aWNhdG9yIl0sImlz
      cyI6Imh0dHBzOlwvXC8xOTIuMTY4LjEwLjE0Mzo5NDQzXC9vYXV0aDJcL3Rva2VuIiwiZXhwIjoxNTY1NjczMDAyLCJpYXQiOjE1NjU2Njk0MDJ9.WoesTXg
      TT4Z1RFOJ352bUKCL-VzYY2wMMfgq6KUTqNUVRhqXEODE0Yc_TxCML9gwNjuOSBK-mX2ZKM4yYKJH9pCLMnHiG6eQfk5Ml_IsgYhJaQMdbHwx-SsTSgbFkll
      rMYPCSqLXpaEW6-YamV7754NzgCC03C6L4SECIIPIfevDgtfOI-QIN6E4onKgoKzGv8C5Zh_hIJ0b6pK6h_jJ4f19OV4BdlpXSAUaWN6VFiQH-nPy3dO5qTR
      sQHrHAqFdgMtwXDm5EgiP18FPfKwpkt4fn8JeZD0eiBdaJZa2EiCe5FQXqqf3P10lr69DyTwbXwcXW8L8rrA6jqmnwNdnsw&
      state=state_1&
      post_logout_redirect_uri=http://192.168.11.196:9092/processing/
    */

    // clear cache
    localStorage.clear()
    // redirect to wso2is
    window.location.href = path

  }

  onSelectLanguage(lang: LanguageItem) {
    this.SelectedLanguage = lang;
    localStorage.setItem(Constants.KEY_LANGUAGE, lang.Key);
    window.location.reload();
  }

  /**
   * @param expireTime the time tokens expire
   */
  countdownTimer() {
    this.expireTime = +localStorage.getItem(Constants.EXPIRE_TIME);
    if (isNaN(this.expireTime)) {
      this.router.navigate(['/auth', { sessionExpirate: 'true' }]);
    }
    this.timer = Observable.timer(0, 86400);
    // subscribing to a observable returns a subscription object
    this.sub = this.timer.subscribe(t => this.checkExpiredToken());
  }

  /**
   * @description Check for expired token
   * if token is expired, redirect to login page
   */
  checkExpiredToken() {
    let now = new Date().getTime();
    if (this.expireTime <= now + 5000) {
      this.authenticationService.getAccessTokenByRefreshToken()
        .then(response => {
          this.tokenInfo = response.data;
          localStorage.setItem(Constants.ACCESS_TOKEN, "Bearer " + this.tokenInfo.accessToken);
          localStorage.setItem(Constants.EXPIRE_TIME, this.tokenInfo.expiresIn + "");
          localStorage.setItem(Constants.REFRESH_TOKEN, this.tokenInfo.refreshToken);
          localStorage.setItem(Constants.IS_AUTHENTIC, "true");
        })
        .catch(error => {
          localStorage.setItem(Constants.IS_AUTHENTIC, "false");
        });
    }
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}

