
import { AdmAccess } from './adm-access';
import { PageInfo2 } from '../../common/util/page-info2';
export class AdmAccessPageInfo extends PageInfo2 {
    content: AdmAccess[];
}