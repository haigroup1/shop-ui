
import { AdmRoleRes } from './adm-role';
import { PageInfo2 } from '../../common/util/page-info2';
export class AdmRolePageInfo extends PageInfo2 {
    content: AdmRoleRes[];
}