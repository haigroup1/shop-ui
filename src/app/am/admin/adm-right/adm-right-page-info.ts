import { PageInfo2 } from '../../common/util/page-info2';
import { AdmRight } from './adm-right';
export class AdmRightPageInfo extends PageInfo2 {
    content: AdmRight[];
}