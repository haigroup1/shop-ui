import { PageInfo2 } from '../../common/util/page-info2';
import { AdmApi } from './adm-api';
export class AdmApiPageInfo extends PageInfo2 {
    content: AdmApi[];
}