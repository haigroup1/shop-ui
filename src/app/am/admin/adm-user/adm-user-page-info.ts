import { AdmUserRes } from './adm-user';
import { PageInfo2 } from '../../common/util/page-info2';

export class AdmUserPageInfo extends PageInfo2 {
    content: AdmUserRes[];
}