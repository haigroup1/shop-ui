import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Constants } from '../../common/util/constants';
/**
 * @description: Define the province form. Use for enter data of the province object
 */
export class LogResponseForm {
    /**
     * @description Define the province form
     * @param fb 
     */
    static logResponseForm(fb: FormBuilder, business: string): FormGroup {
        var logResponseForm: FormGroup;

        logResponseForm = fb.group({

          loai: ["", Validators.compose([
                Validators.required,
            ])],
          heThong: ["", Validators.compose([
                Validators.required,
            ])],
          fromDate: ["", Validators.compose([
              Validators.required,

          ])],
          toDate: ["", Validators.compose([
              Validators.required,
          ])],
          loaiDoiSoat: ["", Validators.compose([
            Validators.required,
          ])],
        });
        return logResponseForm;
    }

    // static bindingDataNonRef(provinceForm: FormGroup, province: Province, createdTime: any) {

    //     provinceForm.patchValue({
    //         id: province.id,
    //         code: province.code,
    //         name: province.name,
    //         issue_modification_decision: province.issue_modification_decision,
    //         issued_decision_date: createdTime,
    //         issued_department: province.issued_department,
    //         englishName: province.englishName,
    //         status: province.status,
    //         updated_time: province.updated_time
    //     });

    // }
}
