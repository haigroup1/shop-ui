import { Injectable } from "@angular/core";

import { Http, Headers, Response } from '@angular/http';

import { Router } from "@angular/router";
import { AppConfig } from "../../../app.config";
import { CommonService } from "../../common/util/common-service/common.service";
import { HeaderField } from "../../common/util/header-field";
import { HeaderValue } from "../../common/util/header-value";

@Injectable()
export class logResponseService extends CommonService {

    logResponseApi = AppConfig.settings.API_URL + "log/amountLog";
    logResponsePageApi = AppConfig.settings.API_URL + "log/amountLogPage";
    logPageAPI = AppConfig.settings.API_URL + "log";
    dmProfileApi = AppConfig.settings.API_URL + "/dmProfile";
    tthccApi = AppConfig.settings.API_URL + "/tthccapi";

    constructor(
        private http: Http,
        router: Router
    ) {
        super(router)
    }

    getPagelogResponse(logResponse: any): Promise<any> {
        let accessToken = this.getAccessToken();
        var secureHeaders = new Headers();
        // secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
        secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
        var searchObject=encodeURI(JSON.stringify(logResponse))
        var promise = this.http.get(this.logResponseApi + "?search="+searchObject, { headers: secureHeaders })
        .toPromise()
        .then(response => response.json() as any)
        .catch(error => {
            return this.handleError(error);
        });

        return promise;
    }


  getPagelogCompare(search: any, page: number): Promise<any> {
    let accessToken = this.getAccessToken();
    var secureHeaders = new Headers();
    // secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    var x=encodeURI(JSON.stringify(search))
    var promise = this.http.get(this.logResponsePageApi + "?search="+x + '&page=' + page + '&size=' + 10, { headers: secureHeaders })
      .toPromise()
      .then(response => response.json() as any)
      .catch(error => {
        return this.handleError(error);
      });

    return promise;
  }

  getLogByDate(item: any, type_detail: string, page: number): Promise<any> {
    let accessToken = this.getAccessToken();
    var secureHeaders = new Headers();
    // secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    var x=encodeURI(JSON.stringify(item));
    var promise = this.http.get(this.logResponseApi + "/listLogByDate?search="+x +'&type='+type_detail+ '&page=' + page + '&size=' + 10, { headers: secureHeaders })
      .toPromise()
      .then(response => response.json() as any)
      .catch(error => {
        return this.handleError(error);
      });
    return promise;
  }

  getLogPage(item: any, page: number): Promise<any> {
    let accessToken = this.getAccessToken();
    var secureHeaders = new Headers();
    // secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    var x=encodeURI(JSON.stringify(item));
    var promise = this.http.get(this.logPageAPI+ "?search="+x + '&page=' + page + '&size=' + 10, { headers: secureHeaders })
      .toPromise()
      .then(response => response.json() as any)
      .catch(error => {
        return this.handleError(error);
      });
    return promise;
  }

  getListType(type: any): Promise<any> {
    let accessToken = this.getAccessToken();
    var secureHeaders = new Headers();
    // secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    var promise = this.http.get(this.dmProfileApi + "/findByType?type=" + type, { headers: secureHeaders })
      .toPromise()
      .then(response => response.json() as any)
      .catch(error => {
        return this.handleError(error);
      });
    return promise;
  }

  tthccApiList(): Promise<any> {
    let accessToken = this.getAccessToken();
    var secureHeaders = new Headers();
    // secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    var promise = this.http.get(this.tthccApi + "/list", { headers: secureHeaders })
      .toPromise()
      .then(response => response.json() as any)
      .catch(error => {
        return this.handleError(error);
      });
    return promise;
  }

  getLogById(id: any): Promise<any> {
    let accessToken = this.getAccessToken();
    var secureHeaders = new Headers();
    // secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
    secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
    var promise = this.http.get(this.logPageAPI + "/detail?id=" + id, { headers: secureHeaders })
      .toPromise()
      .then(response => response.json() as any)
      .catch(error => {
        return this.handleError(error);
      });
    return promise;
  }

}
