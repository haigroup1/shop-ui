export class logResponse {
  loai: String;
  heThong: String;
  fromDate: Date;
  toDate: Date;
  loaiDoiSoat: string;

  public logSend() {
    this.loai = "";
    this.heThong = "";
    this.fromDate = null;
    this.toDate = null;
    this.loaiDoiSoat = "SEND"
    return this;
  }

  public logReceived() {
    this.loai = "";
    this.heThong = "";
    this.fromDate = null;
    this.toDate = null;
    this.loaiDoiSoat = "RECEIVED"
    return this;
  }
}
