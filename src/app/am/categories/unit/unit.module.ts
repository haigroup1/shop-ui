import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule, Router } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTableModule } from "angular2-datatable";
import { ResponseMessageModule } from '../../common/util/response-message/response-message.module';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';
import { createTranslateLoader, CustomHandler } from '../../../i18n-setting';
import { HttpClient } from '@angular/common/http';
import { AuthGuardSubmenu } from '../../../authentication/guard/auth.guard-submenu';
import { SelectModule } from 'ng2-select';
import { ChartsModule } from 'ng2-charts';
import { UnitComponent } from './unit.component';


const routes: Routes = [

  { path: '', component: UnitComponent, pathMatch: 'full' },
//   { path: 'detail/:id', component: DistrictDetailComponent, pathMatch: 'full' },
  // { path: ':business/:id', canActivate: [AuthGuardSubmenu], component: DistrictBusinessComponent, pathMatch: 'full' },
//   { path: 'reference/:id/list', canActivate: [AuthGuardSubmenu], component: DistrictRefListComponent, pathMatch: 'full' },
//   { path: 'reference/:business/:id', canActivate: [AuthGuardSubmenu], component: DistrictRefBusinessComponent, pathMatch: 'full' },
]

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    ResponseMessageModule,
    DataTableModule,
    SelectModule,
    ChartsModule,
    ToastModule.forRoot(),
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      },
      missingTranslationHandler: { provide: MissingTranslationHandler, useClass: CustomHandler },
      isolate: false
    })
  ],
  declarations: [
    UnitComponent,
  ],
  exports: [RouterModule],
  providers: [AuthGuardSubmenu]
})
export class UnitModule { }
