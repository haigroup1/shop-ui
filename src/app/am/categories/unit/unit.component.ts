import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Toast, ToastsManager } from 'ng2-toastr';
import { AuthGuardSubmenu } from '../../../authentication/guard/auth.guard-submenu';
import { DialogService } from '../../common/dialog/dialog.service';
import { PageInfo } from '../../common/util/page-info';
import { unit } from './unit';
import { unitService } from './unit.service';
import { DataTable } from 'angular2-datatable';

@Component({
  selector: 'app-unit',
  templateUrl: './unit.component.html',
  styleUrls: ['./unit.component.css'],
  providers: [unitService, DataTable, DialogService]
})
export class UnitComponent implements OnInit {

  unitInfo: PageInfo<unit[]>
  currentPage = 0
  filterForm: FormGroup
  // search restriction
  searchObject: unit
  checkAllItemFlag = false
  currentPageView: number = 0
  fromElement: number
  toElement: number
  // total page
  totalPages: number
  // page sizw
  pageLength: number
  // toal elements
  totalElements: number
  numberDeleteItems = 0

  provinceSelections: Array<any> = []
  indexProvinceSelection: number

  listStatus = null;

  // list units to export file excel
  units: unit[]

  isRef: boolean = false
  numberRefItems: number = 0
  idunitCountRef: string
  checkAllItemFlagRef = false

  constructor(
    private unitService: unitService,
    private dialogService: DialogService,
    private fb: FormBuilder,
    private router: Router,
    private translate: TranslateService,
    private authGuardSubmenu: AuthGuardSubmenu,
    public toastr: ToastsManager, vcr: ViewContainerRef,

  ) { }

  ngOnInit() {
    this.filterForm = new FormGroup({
      name: new FormControl(),
      centerCode: new FormControl(),
      code: new FormControl(),
      description: new FormControl(),
    });
    debugger
    this.unitInfo = new PageInfo<unit[]>();
    this.searchObject = new unit()
    this.getPageUnit(this.searchObject, this.currentPage)
  }
  getPageUnit(unit: unit, page: number) {
    debugger
    this.searchObject = unit;
    this.unitService.getPageUnit(unit, page)
      .then(response => {
        debugger
        this.unitInfo = response.data;
        this.units = this.unitInfo.content;
        console.log(this.units)
        this.pageLength = this.unitInfo.content.length;
        this.totalElements = this.unitInfo.totalElements;
        this.totalPages = this.unitInfo.totalPages;
        if (!(this.totalPages > 0)) {
          this.currentPage = -1;
        }

        this.setCurrentPage()
      }).catch(
        error => {
          console.log("no ok");
          console.log(error);
        });


  }

  /**
   * @description: Manage page transfers
   * @param page: Page will move to
   */
  choosePageNumber(page) {
    debugger;
    var flag = true;
    var pageNumber;

    if (page.valueAsNumber != null) {
      if (isNaN(page.valueAsNumber)) {
        flag = false;
        page.value = this.currentPage + 1;
        // this.currentPageView = 1;
      } else {
        pageNumber = page.value - 1;
      }
    } else {
      pageNumber = page;
    }

    if (flag == true && this.currentPage > pageNumber && pageNumber < 0) {
      pageNumber = 0;
    }
    if (flag == true && this.currentPage < pageNumber && pageNumber > this.totalPages - 1) {
      pageNumber = this.totalPages - 1;
    }
    if (flag == true && !Number.isInteger(pageNumber)) {
      flag = false;
      page.value = this.currentPage + 1;
    }
    if (flag == true) {

      this.currentPage = pageNumber;
      this.getPageUnit(this.searchObject, this.currentPage);
      page.value = pageNumber + 1;
    }
  }

  // set the information of the page
  private setCurrentPage() {
    if (this.unitInfo.totalElements > 0) {
      this.currentPageView = this.unitInfo.number + 1;
    }
    var numberOfElements = this.unitInfo.numberOfElements;
    var size = this.unitInfo.size;
    this.fromElement = this.unitInfo.number * size + 1;
    this.toElement = this.unitInfo.number * size + numberOfElements;
    if (this.toElement < 1) {
      this.fromElement = 0;
      this.toElement = 0;
    }
  }

  provinceChanged(id: string) {
    this.filterForm.get('province.id').setValue(id)

  }


  // routerLinkUpdate(type) {
  //     debugger
  //     if (type == 'create') {
  //         this.router.navigate(['/unit/create']);
  //     } else {
  //         this.unitInfo.content.forEach(item => {
  //             if (item.checked == true) {
  //                 debugger
  //                 this.router.navigate(['/unit/' + type, item.id]);
  //             }
  //         });
  //     }
  // }

  /**
   * @description: export list unit to excel
   */
  // To Authorize User
  isAuthoriziedNavigation(): boolean {
    var isAuthorizied = this.authGuardSubmenu.isAuthoriziedWithCurrentUrl(this.router.url);
    return isAuthorizied;
  }


  success(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.success('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

  notice(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.warning('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

  error(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.error('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

}
