import { Injectable } from "@angular/core";

import { Http, Headers, Response } from '@angular/http';

import { Router } from "@angular/router";
import { AppConfig } from "../../../app.config";
import { CommonService } from "../../common/util/common-service/common.service";
import { Constants } from "../../common/util/constants";
import { HeaderField } from "../../common/util/header-field";
import { HeaderValue } from "../../common/util/header-value";
import { unit } from "./unit";

@Injectable()
export class unitService extends CommonService { 

    UnitAPI = AppConfig.settings.API_URL + "unit";

    constructor(
        private http: Http,
        router: Router
    ) {
        super(router)
    }

    getPageUnit(unit: unit, page: number): Promise<any> {
        debugger
        let accessToken = this.getAccessToken();
        var secureHeaders = new Headers();
        secureHeaders.append(HeaderField.AUTHORIZATION, accessToken);
        secureHeaders.append(HeaderField.CONTENT_TYPE, HeaderValue.APPLICATION_JSON_VALUE);
        var promise = this.http.post(this.UnitAPI + "/search?page=" + page + "&size=" + Constants.PAGE_SIZE,unit, { headers: secureHeaders })
            .toPromise()
            .then(response => response.json() as any)
            .catch(error => {
                return this.handleError(error);
            });
        return promise;
    }

}