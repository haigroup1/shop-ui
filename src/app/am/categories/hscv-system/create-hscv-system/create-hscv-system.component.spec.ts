import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateHscvSystemComponent } from './create-hscv-system.component';

describe('CreateHscvSystemComponent', () => {
  let component: CreateHscvSystemComponent;
  let fixture: ComponentFixture<CreateHscvSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateHscvSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateHscvSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
