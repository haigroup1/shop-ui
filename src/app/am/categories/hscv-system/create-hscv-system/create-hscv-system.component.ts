import { Location } from '@angular/common';
import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastsManager, Toast } from 'ng2-toastr';
import { HSCVSystem } from '../hscv-system';
import { HSCVSystemForm } from '../hscv-system-form.component';
import { HSCVSystemService } from '../hscv-system.service';

@Component({
  selector: 'app-create-hscv-system',
  templateUrl: './create-hscv-system.component.html',
  styleUrls: ['./create-hscv-system.component.css']
})
export class CreateHscvSystemComponent implements OnInit {

  translate: TranslateService;
  filterForm: FormGroup;
  hscvSystem: HSCVSystem;
  toasts: any;
  dialogRef: any;

  constructor(
    public toastr: ToastsManager, vcr: ViewContainerRef,
    private location: Location,
    private fb: FormBuilder,
    private router: Router,
    private hscvSystemService: HSCVSystemService
  ) {
    this.toastr.setRootViewContainerRef(vcr)
   }

  ngOnInit() {
    this.filterForm = HSCVSystemForm.CreateForm(this.fb);
    this.hscvSystem = new HSCVSystem();
  }

  isValidForm() {
    if (!this.filterForm.get('maHT').valid || this.filterForm.get('maHT').value.trim()=='') {
      return false
    }
    // if (!this.filterForm.get('tenHT').valid || this.filterForm.get('tenHT').value.trim()=='') {
    //   return false
    // }
    return true
  }

  goBack() {
    this.location.back();
  }

  submit(hscvSystem: HSCVSystem) {
    debugger
    this.hscvSystemService.create(hscvSystem)
      .then(response => {
        this.toastr.success('Thêm mới hệ thống hồ sơ công việc thành công')
        .then((toast: Toast) => {
          setTimeout(() => {
            this.goBack();
          }, 2000);
        })
      })
      .catch(error => {
        let message;
        this.translate.get('Message.CreateFail').subscribe((res: string) => {
          message = res;
        });
        this.toastr.error('', message, { dismiss: 'controlled' })
          .then((toast: Toast) => {
            setTimeout(() => {
              this.toastr.dismissToast(toast);
            }, 3000);
          });
      });
  }
}
