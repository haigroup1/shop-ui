import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Constants } from '../../common/util/constants';
import { HSCVSystem } from './hscv-system';

/**
 * @description: Define the province form. Use for enter data of the province object
 */
export class HSCVSystemForm {
    /**
     * @description Define the province form
     * @param fb 
     */
    static hscvSystemForm(fb: FormBuilder, business: string): FormGroup {
        var hscvSystemForm: FormGroup;

        hscvSystemForm = fb.group({
            id: [],
            maHT: [],
            tenHT: [],
            status: [],
        });
        return hscvSystemForm;
    }

    /**
     * @description Định nghĩa form tạo
     * @param fb 
     */
  static CreateForm(fb: FormBuilder): FormGroup {
    var filterForm: FormGroup;

    filterForm = fb.group({
        id: 0,
        maHT: ["", Validators.compose([
            Validators.required,
        ])],
        tenHT: ["", Validators.compose([
          Validators.required,
        ])],
        status: 1
    })
    return filterForm
  }

    static bindingData(hscvSystemForm: FormGroup, agency: HSCVSystem) {
        
      hscvSystemForm.patchValue({
            id: agency.id,
            maHT: agency.maHT,
            tenHT: agency.tenHT,
            status: agency.status
        });
        
    }


}
