import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Toast, ToastsManager } from 'ng2-toastr';
import { AuthGuardSubmenu } from '../../../authentication/guard/auth.guard-submenu';
import { DialogService } from '../../common/dialog/dialog.service';
import { PageInfo } from '../../common/util/page-info';
import { HSCVSystem } from './hscv-system';
import { HSCVSystemService } from './hscv-system.service';
import { DataTable } from 'angular2-datatable';
import { Constants } from '../../common/util/constants';

@Component({
  selector: 'app-hscv-system',
  templateUrl: './hscv-system.component.html',
  styleUrls: ['./hscv-system.component.css'],
  providers: [HSCVSystemService, DataTable, DialogService]
})
export class HSCVSystemComponent implements OnInit {

  hscvSystemInfo: PageInfo<HSCVSystem[]>
  currentPage = 0
  filterForm: FormGroup
  // search restriction
  searchObject: HSCVSystem
  checkAllItemFlag = false
  currentPageView: number = 0
  fromElement: number
  toElement: number
  // total page
  totalPages: number
  // page sizw
  pageLength: number
  // toal elements
  totalElements: number
  numberDeleteItems = 0
  listStatusSystem = Constants.SYSTEM_STATUS;

  // list tthccSystems to export file excel
  hscvSystems: HSCVSystem[]

  isRef: boolean = false
  numberRefItems: number = 0
  idtHSCVSystemCountRef: string
  checkAllItemFlagRef = false

  constructor(
    private hscvSystemService: HSCVSystemService,
    private dialogService: DialogService,
    private fb: FormBuilder,
    private router: Router,
    private translate: TranslateService,
    private authGuardSubmenu: AuthGuardSubmenu,
    public toastr: ToastsManager, vcr: ViewContainerRef,

  ) {
    this.toastr.setRootViewContainerRef(vcr);
   }

  ngOnInit() {
    this.filterForm = new FormGroup({
      maHT: new FormControl(),
      tenHT: new FormControl(),
      status: new FormControl(2),
    });
    debugger
    this.hscvSystemInfo = new PageInfo<HSCVSystem[]>();
    this.searchObject = new HSCVSystem()
    this.searchHSCVSystem(this.searchObject, this.currentPage)
  }
  searchHSCVSystem(hscvSystem: HSCVSystem, page: number) {
    debugger
    this.searchObject = hscvSystem;
    this.hscvSystemService.getPageHSCVSystem(hscvSystem, page)
      .then(response => {
        debugger
        this.hscvSystemInfo = response.data;
        this.hscvSystems = this.hscvSystemInfo.content;
        console.log(this.hscvSystems)
        this.pageLength = this.hscvSystemInfo.content.length;
        this.totalElements = this.hscvSystemInfo.totalElements;
        this.totalPages = this.hscvSystemInfo.totalPages;
        if (!(this.totalPages > 0)) {
          this.currentPage = -1;
        }

        this.setCurrentPage()
      }).catch(
        error => {
          console.log("no ok");
          console.log(error);
        });
  }

  // deleteOneItem(id: number) {
  //   this.delete(id);
  // }

  // private delete(entityIds: number) {
  //   debugger;
  //   this.dialogService
  //     .confirm('Dialog.ConfirmInfo', 'Dialog.MessageConfirm')
  //     .subscribe(response => {
  //       if (response == true) {
  //         this.hscvSystemService.deleteB(entityIds)
  //           .then(response => {
  //             let message;
  //             if (response.code == 200) {
  //               this.translate.get('Message.DeleteSuccess').subscribe((res: string) => {
  //                 message = res;
  //               });
  //               this.toastr.success('', message, { dismiss: 'controlled' })
  //                 .then((toast: Toast) => {
  //                   setTimeout(() => {
  //                     this.toastr.dismissToast(toast);
  //                   }, 3000);
  //                 });
  //             } else if (response.code == 400) {
  //               this.translate.get('Message.DeleteFail').subscribe((res: string) => {
  //                 message = res;
  //               });
  //               this.toastr.error('', message, { dismiss: 'controlled' })
  //                 .then((toast: Toast) => {
  //                   setTimeout(() => {
  //                     this.toastr.dismissToast(toast);
  //                   }, 3000);
  //                 });
  //             }

  //             this.searchHSCVSystem(this.searchObject, this.currentPage);
  //           })
  //           .catch(error => {
  //             let message;
  //             this.translate.get('Message.DeleteFail').subscribe((res: string) => {
  //               message = res;
  //             });
  //             this.toastr.error('', message, { dismiss: 'controlled' })
  //               .then((toast: Toast) => {
  //                 setTimeout(() => {
  //                   this.toastr.dismissToast(toast);
  //                 }, 3000);
  //               });
  //           });
  //       }
  //     })
  // }


  /**
   * @description: Manage page transfers
   * @param page: Page will move to
   */
  choosePageNumber(page) {
    debugger;
    var flag = true;
    var pageNumber;

    if (page.valueAsNumber != null) {
      if (isNaN(page.valueAsNumber)) {
        flag = false;
        page.value = this.currentPage + 1;
        // this.currentPageView = 1;
      } else {
        pageNumber = page.value - 1;
      }
    } else {
      pageNumber = page;
    }

    if (flag == true && this.currentPage > pageNumber && pageNumber < 0) {
      pageNumber = 0;
    }
    if (flag == true && this.currentPage < pageNumber && pageNumber > this.totalPages - 1) {
      pageNumber = this.totalPages - 1;
    }
    if (flag == true && !Number.isInteger(pageNumber)) {
      flag = false;
      page.value = this.currentPage + 1;
    }
    if (flag == true) {

      this.currentPage = pageNumber;
      this.searchHSCVSystem(this.searchObject, this.currentPage);
      page.value = pageNumber + 1;
    }
  }

  // set the information of the page
  private setCurrentPage() {
    if (this.hscvSystemInfo.totalElements > 0) {
      this.currentPageView = this.hscvSystemInfo.number + 1;
    }
    var numberOfElements = this.hscvSystemInfo.numberOfElements;
    var size = this.hscvSystemInfo.size;
    this.fromElement = this.hscvSystemInfo.number * size + 1;
    this.toElement = this.hscvSystemInfo.number * size + numberOfElements;
    if (this.toElement < 1) {
      this.fromElement = 0;
      this.toElement = 0;
    }
  }


  routerLinkUpdate(type) {
    debugger
    if (type == 'create') {
      debugger
      this.router.navigate(['/hscv/create']);
    } else {
      this.hscvSystemInfo.content.forEach(item => {
        if (item.checked == true) {
          debugger
          this.router.navigate(['/hscv/' + type, item.id]);
        }
      });
    }
  }

  /**
   * @description: export list tthccSystem to excel
   */
  // To Authorize User
  isAuthoriziedNavigation(): boolean {
    var isAuthorizied = this.authGuardSubmenu.isAuthoriziedWithCurrentUrl(this.router.url);
    return isAuthorizied;
  }


  success(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.success('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

  notice(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.warning('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

  error(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.error('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

}

