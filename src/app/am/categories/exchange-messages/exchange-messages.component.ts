import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {logResponseService} from '../log-response/logResponse.service';
import {tthccSystemService} from '../tthcc-system/tthcc-system.service';
import {DialogService} from '../../common/dialog/dialog.service';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AuthGuardSubmenu} from '../../../authentication/guard/auth.guard-submenu';
import {ToastsManager} from 'ng2-toastr';
import {DataTable} from 'angular2-datatable';
import {AdmApiService} from '../../admin/adm-api/adm-api.service';
import {HSCVSystemService} from '../hscv-system/hscv-system.service';

@Component({
  selector: 'app-exchange-messages',
  templateUrl: './exchange-messages.component.html',
  providers:[logResponseService, DialogService, DataTable, HSCVSystemService, AdmApiService],
})
export class ExchangeMessagesComponent implements OnInit {

  searchObject = {loai: "", maHTGui: "", maHTNhan: "", fromDate: null, toDate: null, type_request: "",maHSGui: "", maHSNhan: "" };
  currentPage = 0;

  checkAllItemFlag = false;
  currentPageView: number = 0;
  fromElement: number;
  toElement: number;
  // total page
  totalPages: number;
  // page sizw
  pageLength: number;
  // toal elements
  totalElements: number;
  logResponses: any[] = [];
  logResponseInfo: any;

  listSystem: any[] = [];

  listLoai = [
    {id: 'NDCV', text: 'NDCV'},
    {id: 'NDYK', text: 'NDYK'},
    {id: 'KQCV', text: 'KQCV'},
    {id: 'DUTHAO', text: 'DUTHAO'},
    {id: 'STATUS', text: 'STATUS'},
    {id: 'BCCVQH', text: 'BCCVQH'},
    {id: 'CVTH', text: 'CVTH'},
    {id: 'DMCV', text: 'DMCV'}
  ];

  listTypeRequest = [
    {id: 'SAVE', text: 'sendProfile'},
    {id: 'GET', text: 'getProfile'},
    {id: 'UPDATE', text: 'updateReceiver'},
  ]
  logById: any = {data_input_o: {}, data_output_o: {}, input_str: '', output_str: ''};
  isJson_data_input_o: boolean = true;
  isJson_data_output_o: boolean = true;

  constructor(private logResponseService: logResponseService,
              private hscvSystemService: HSCVSystemService,
              private dialogService: DialogService,
              private fb: FormBuilder,
              private router: Router,
              private translate: TranslateService,
              private authGuardSubmenu: AuthGuardSubmenu,
              public toastr: ToastsManager, vcr: ViewContainerRef,
              ) { }

  ngOnInit() {
    this.getLogByDate(this.currentPage);
    this.getListSysteam();
  }

  getListSysteam() {
    this.hscvSystemService.getListHSCVSystem()
      .then(response => {
        debugger
        console.log(response);
        this.listSystem = response.data;
      }).catch(
      error => {
        console.log("no ok");
        console.log(error);
      });
  }


  private getLogById(id) {
    this.logById = {data_input_o: {}, data_output_o: {}, input_str: '', output_str: ''};
    this.logResponseService.getLogById(id)
      .then(response => {

        // this.logById = response;
        debugger
        try {
          JSON.parse(response.data.data_input);
          this.isJson_data_input_o = true;
          this.logById.data_input_o = response.data.data_input;
        } catch (e) {
          this.isJson_data_input_o = false;
          this.logById.input_str = response.data.data_input;
        }

        try {
          JSON.parse(response.data.data_output);
          this.isJson_data_output_o = true;
          this.logById.data_output_o = response.data.data_output;
        } catch (e) {
          this.isJson_data_output_o = false;
          this.logById.output_str = response.data.data_output;
        }

      }).catch(error => {
      console.log("no ok");
      console.log(error);
    });
  }

  private getLogByDate(page: number) {
    if(typeof this.searchObject.toDate == 'undefined' || this.searchObject.toDate==null || this.searchObject.toDate.toString()=='') this.searchObject.toDate= null
    if(typeof this.searchObject.fromDate == 'undefined' || this.searchObject.fromDate==null || this.searchObject.fromDate.toString()=='') this.searchObject.fromDate= null

    this.logResponseService.getLogPage(this.searchObject, page)
      .then(response => {

        this.logResponseInfo = response.data;
        console.log(response);
        this.logResponses = this.logResponseInfo.content;
        this.pageLength = this.logResponseInfo.content.length;
        this.totalElements = this.logResponseInfo.totalElements;
        this.totalPages = this.logResponseInfo.totalPages;
        if (!(this.totalPages > 0)) {
          this.currentPage = -1;
        }
        this.setCurrentPage();
      }).catch(
      error => {
        console.log("no ok");
        console.log(error);
      });
  }

  choosePageNumber(page) {
    var flag = true;
    var pageNumber;

    if (page.valueAsNumber != null) {
      if (isNaN(page.valueAsNumber)) {
        flag = false;
        page.value = this.currentPage + 1;
        // this.currentPageView = 1;
      } else {
        pageNumber = page.value - 1;
      }
    } else {
      pageNumber = page;
    }

    if (flag == true && this.currentPage > pageNumber && pageNumber < 0) {
      pageNumber = 0;
    }
    if (flag == true && this.currentPage < pageNumber && pageNumber > this.totalPages - 1) {
      pageNumber = this.totalPages - 1;
    }
    if (flag == true && !Number.isInteger(pageNumber)) {
      flag = false;
      page.value = this.currentPage + 1;
    }
    if (flag == true) {

      this.currentPage = pageNumber;
      this.getLogByDate(this.currentPage);

      page = pageNumber + 1;
    }
  }

  private setCurrentPage() {
    if (this.logResponseInfo.totalElements > 0) {
      this.currentPageView = this.logResponseInfo.number + 1;
    }
    var numberOfElements = this.logResponseInfo.numberOfElements;
    var size = this.logResponseInfo.size;
    this.fromElement = this.logResponseInfo.number * size + 1;
    this.toElement = this.logResponseInfo.number * size + numberOfElements;
    if (this.toElement < 1) {
      this.fromElement = 0;
      this.toElement = 0;
    }
  }

}
