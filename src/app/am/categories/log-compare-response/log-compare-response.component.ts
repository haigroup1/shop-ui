import {Component, OnInit, ViewContainerRef} from '@angular/core';
import {logResponseService} from '../log-response/logResponse.service';
import {DialogService} from '../../common/dialog/dialog.service';
import {FormBuilder} from '@angular/forms';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AuthGuardSubmenu} from '../../../authentication/guard/auth.guard-submenu';
import {ToastsManager} from 'ng2-toastr';
import {DataTable} from 'angular2-datatable';
import {tthccSystemService} from '../tthcc-system/tthcc-system.service';
import {HSCVSystemService} from '../hscv-system/hscv-system.service';

@Component({
  selector: 'app-log-compare-response',
  templateUrl: './log-compare-response.component.html',
  providers:[logResponseService, DialogService, DataTable, tthccSystemService, HSCVSystemService],
})
export class LogCompareResponseComponent implements OnInit {

  searchObject = {loaiDoiSoat: "RECEIVED", loai: "", heThong: "", fromDate: null, toDate: null, ma_hs_gui: "", ma_hs_nhan: "",  ngaytao : ""};
  currentPage = 0;

  checkAllItemFlag = false;
  currentPageView: number = 0;
  fromElement: number;
  toElement: number;
  // total page
  totalPages: number;
  // page sizw
  pageLength: number;
  // toal elements
  totalElements: number;
  logResponses: any[] = [];
  logResponseInfo: any;

  dataInfos: any;
  dataInfo: any[];
  _pageLength: number;
  _totalElements: number;
  _totalPages: number;
  _currentPage = 0;
  _currentPageView: number = 0;
  _fromElement: number;
  _toElement: number;
  itemSearch: any;
  type_detail: any;

  listType = [
    {id: 'NDCV', text: 'NDCV'},
    {id: 'NDYK', text: 'NDYK'},
    {id: 'KQCV', text: 'KQCV'},
    {id: 'DUTHAO', text: 'DUTHAO'},
    {id: 'STATUS', text: 'STATUS'},
    {id: 'BCCVQH', text: 'BCCVQH'},
    {id: 'CVTH', text: 'CVTH'},
    {id: 'DMCV', text: 'DMCV'}
  ];
  listStatus: any[] = [];
  listSystem: any[] = [];
  listApi: any[] = [];

  constructor(private logResponseService: logResponseService,
              private tthccSystemService: tthccSystemService,
              private dialogService: DialogService,
              private fb: FormBuilder,
              private router: Router,
              private translate: TranslateService,
              private authGuardSubmenu: AuthGuardSubmenu,
              public toastr: ToastsManager, vcr: ViewContainerRef,
              private hscvSystemService: HSCVSystemService,) {}

  ngOnInit() {
    this.getPagelogCompare(this.currentPage);
    this.getListSysteam();
  }

  getListSysteam() {
    this.hscvSystemService.getListHSCVSystem()
      .then(response => {
        debugger
        console.log(response);
        this.listSystem = response.data;
      }).catch(
      error => {
        console.log("no ok");
        console.log(error);
      });
  }


  // private changeType(form) {
  //   if (form.type != '') {
  //
  //     this.logResponseService.getListType(form.type)
  //       .then(response => {
  //
  //         console.log(response);
  //         this.listStatus = response;
  //
  //       }).catch(error => {
  //       console.log("no ok");
  //       console.log(error);
  //     });
  //
  //   } else {
  //     this.listStatus = [];
  //   }
  // }

  getPagelogCompare(page: number) {
    this.searchObject.loaiDoiSoat = "RECEIVED";
    if (typeof this.searchObject.toDate == 'undefined' || this.searchObject.toDate == null || this.searchObject.toDate.toString() == '') this.searchObject.toDate = null;
    if (typeof this.searchObject.fromDate == 'undefined' || this.searchObject.fromDate == null || this.searchObject.fromDate.toString() == '') this.searchObject.fromDate = null;

    this.logResponseService.getPagelogCompare(this.searchObject, page)
      .then(response => {

        this.logResponseInfo = response.data;

        this.logResponses = this.logResponseInfo.content;
        this.pageLength = this.logResponseInfo.content.length;
        this.totalElements = this.logResponseInfo.totalElements;
        this.totalPages = this.logResponseInfo.totalPages;
        if (!(this.totalPages > 0)) {
          this.currentPage = -1;
        }
        this.setCurrentPage();
      }).catch(
      error => {
        console.log("no ok");
        console.log(error);
      });
  }

  getLogByDate(item, type_detail, page: number) {
    this.itemSearch = item;
    this.type_detail = type_detail;

    this.searchObject.ngaytao = item[0];
    // this.searchObject.dateSearch = item.creation_date_sub;
    // this.searchObject.type_detail = type_detail;

    this.dataInfos = null;
    this.dataInfo = [];

    this.logResponseService.getLogByDate(this.searchObject, type_detail, page)
      .then(response => {

        this.dataInfos = response.data;

        this.dataInfo = this.dataInfos.content;
        this._pageLength = this.dataInfos.content.length;
        this._totalElements = this.dataInfos.totalElements;
        this._totalPages = this.dataInfos.totalPages;

        if (!(this._totalPages > 0)) {
          this._currentPage = -1;
        }
        this._setCurrentPage();

      }).catch(error => {
      console.log("no ok");
      console.log(error);
    });
  }

  choosePageNumber(page) {
    var flag = true;
    var pageNumber;

    if (page.valueAsNumber != null) {
      if (isNaN(page.valueAsNumber)) {
        flag = false;
        page.value = this.currentPage + 1;
        // this.currentPageView = 1;
      } else {
        pageNumber = page.value - 1;
      }
    } else {
      pageNumber = page;
    }

    if (flag == true && this.currentPage > pageNumber && pageNumber < 0) {
      pageNumber = 0;
    }
    if (flag == true && this.currentPage < pageNumber && pageNumber > this.totalPages - 1) {
      pageNumber = this.totalPages - 1;
    }
    if (flag == true && !Number.isInteger(pageNumber)) {
      flag = false;
      page.value = this.currentPage + 1;
    }
    if (flag == true) {

      this.currentPage = pageNumber;
      this.getPagelogCompare(this.currentPage);

      page = pageNumber + 1;
    }
  }

  private setCurrentPage() {
    if (this.logResponseInfo.totalElements > 0) {
      this.currentPageView = this.logResponseInfo.number + 1;
    }
    var numberOfElements = this.logResponseInfo.numberOfElements;
    var size = this.logResponseInfo.size;
    this.fromElement = this.logResponseInfo.number * size + 1;
    this.toElement = this.logResponseInfo.number * size + numberOfElements;
    if (this.toElement < 1) {
      this.fromElement = 0;
      this.toElement = 0;
    }
  }

  private _setCurrentPage() {
    if (this.dataInfos.totalElements > 0) {
      this._currentPageView = this.dataInfos.number + 1;
    }
    var numberOfElements = this.dataInfos.numberOfElements;
    var size = this.dataInfos.size;
    this._fromElement = this.dataInfos.number * size + 1;
    this._toElement = this.dataInfos.number * size + numberOfElements;
    if (this._toElement < 1) {
      this._fromElement = 0;
      this._toElement = 0;
    }
  }

  private _choosePageNumber(page) {
    var flag = true;
    var pageNumber;

    if (page.valueAsNumber != null) {
      if (isNaN(page.valueAsNumber)) {
        flag = false;
        page.value = this._currentPage + 1;
        // this.currentPageView = 1;
      } else {
        pageNumber = page.value - 1;
      }
    } else {
      pageNumber = page;
    }

    if (flag == true && this._currentPage > pageNumber && pageNumber < 0) {
      pageNumber = 0;
    }
    if (flag == true && this._currentPage < pageNumber && pageNumber > this._totalPages - 1) {
      pageNumber = this._totalPages - 1;
    }
    if (flag == true && !Number.isInteger(pageNumber)) {
      flag = false;
      page.value = this._currentPage + 1;
    }
    if (flag == true) {

      this._currentPage = pageNumber;
      this.getLogByDate(this.itemSearch, this.type_detail, this._currentPage);

      page = pageNumber + 1;
    }
  }

}
