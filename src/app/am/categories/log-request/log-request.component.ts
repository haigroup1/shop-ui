import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color,  } from 'ng2-charts';
import { Toast, ToastsManager } from 'ng2-toastr';
import { AuthGuardSubmenu } from '../../../authentication/guard/auth.guard-submenu';
import { DialogService } from '../../common/dialog/dialog.service';
import { PageInfo } from '../../common/util/page-info';
import { logResponse } from '../log-response/logResponse';
import { LogResponseForm } from '../log-response/logResponse-form.component';
import { logResponseService } from '../log-response/logResponse.service';
import {tthccSystemService} from '../tthcc-system/tthcc-system.service';
import {HSCVSystemService} from '../hscv-system/hscv-system.service';
@Component({
  selector: 'app-log-request',
  templateUrl: './log-request.component.html',
  styleUrls: ['./log-request.component.css'],
  providers:[logResponseService, tthccSystemService, HSCVSystemService]
})
export class LogRequestComponent implements OnInit {
  
  count_total: number[] = []
  public lineChartData: ChartDataSets[] = [
    { data: this.count_total, label: 'Log HSCV' },
  ];
  public lineChartLabels: string[]=[];
  public lineChartOptions: (ChartOptions & { annotation: any }) = {
    responsive: true,
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'black',
      backgroundColor: 'rgba(137, 211, 240, 0.57)',
    },
  ];
  public lineChartLegend = true;
  public lineChartType = 'line';
  // public lineChartPlugins = [];

  logResponseInfo: PageInfo<logResponse[]>
  currentPage = 0
  filterForm: FormGroup
  // search restriction
  searchObject: logResponse
  checkAllItemFlag = false
  currentPageView: number = 0
  fromElement: number
  toElement: number
  // total page
  totalPages: number
  // page sizw
  pageLength: number
  // toal elements
  totalElements: number
  numberDeleteItems = 0


  listType = [
    {id:'NDCV', text: 'NDCV'},
    {id:'NDYK', text: 'NDYK'},
    {id:'KQCV', text: 'KQCV'},
    {id:'DUTHAO', text: 'DUTHAO'},
    {id:'STATUS', text: 'STATUS'},
    {id:'BCCVQH', text: 'BCCVQH'},
    {id:'CVTH', text: 'CVTH'},
    {id:'DMCV', text: 'DMCV'}
  ]
  listStatus: any[] = [];
  listSystem: any[] = [];
  // list logResponses to export file excel
  logResponses: logResponse[]

  isRef: boolean = false
  numberRefItems: number = 0
  idlogResponseCountRef: string
  checkAllItemFlagRef = false

  constructor(
    private logResponseService: logResponseService,
    private dialogService: DialogService,
    private fb: FormBuilder,
    private router: Router,
    private translate: TranslateService,
    private authGuardSubmenu: AuthGuardSubmenu,
    public toastr: ToastsManager, vcr: ViewContainerRef,
    private hscvSystemService: HSCVSystemService,

  ) { }

  ngOnInit() {
    this.filterForm = LogResponseForm.logResponseForm(this.fb, '')
    this.searchObject = new logResponse()
    this.searchObject.loaiDoiSoat="SEND";
    this.getPagelogResponse(this.searchObject);
    this.getListSysteam();
  }

  getListSysteam() {
    this.hscvSystemService.getListHSCVSystem()
      .then(response => {
        debugger
        console.log(response);
        this.listSystem = response.data;
      }).catch(
      error => {
        console.log("no ok");
        console.log(error);
      });
  }


  getPagelogResponse(logResponse: logResponse) {
    if(typeof logResponse.toDate == 'undefined' || logResponse.toDate==null || logResponse.toDate.toString()=='') logResponse.toDate= null
    if(typeof logResponse.fromDate == 'undefined' || logResponse.fromDate==null || logResponse.fromDate.toString()=='') logResponse.fromDate= null
    this.searchObject = logResponse;
    this.lineChartLabels=[]
    this.searchObject.loaiDoiSoat="SEND";
    this.logResponseService.getPagelogResponse(this.searchObject)
      .then(response => {
        debugger
        console.log(response)
      
        this.lineChartData=[]
        this.count_total=[]
        response.data.forEach(element => {
          this.count_total.push(element[1]);
          this.lineChartLabels.push(element[0])
        });
console.log(this.lineChartLabels)
        this.lineChartData = [
          { data: this.count_total, label: 'Log HSCV' },
        ];
      }).catch(
        error => {
          console.log("no ok");
          console.log(error);
        });


  }



  success(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.success('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

  notice(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.warning('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

  error(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.error('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

}
