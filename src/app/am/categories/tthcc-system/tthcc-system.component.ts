import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Toast, ToastsManager } from 'ng2-toastr';
import { AuthGuardSubmenu } from '../../../authentication/guard/auth.guard-submenu';
import { DialogService } from '../../common/dialog/dialog.service';
import { PageInfo } from '../../common/util/page-info';
import { tthccSystem } from './tthcc-system';
import { tthccSystemService } from './tthcc-system.service';
import { DataTable } from 'angular2-datatable';
import { Constants } from '../../common/util/constants';

@Component({
  selector: 'app-tthcc-system',
  templateUrl: './tthcc-system.component.html',
  styleUrls: ['./tthcc-system.component.css'],
  providers: [tthccSystemService, DataTable, DialogService]
})
export class TthccSystemComponent implements OnInit {

  tthccSystemInfo: PageInfo<tthccSystem[]>
  currentPage = 0
  filterForm: FormGroup
  // search restriction
  searchObject: tthccSystem
  checkAllItemFlag = false
  currentPageView: number = 0
  fromElement: number
  toElement: number
  // total page
  totalPages: number
  // page sizw
  pageLength: number
  // toal elements
  totalElements: number
  numberDeleteItems = 0
  listStatusSystem = Constants.SYSTEM_STATUS;

  // list tthccSystems to export file excel
  tthccSystems: tthccSystem[]

  isRef: boolean = false
  numberRefItems: number = 0
  idtthccSystemCountRef: string
  checkAllItemFlagRef = false

  constructor(
    private tthccSystemService: tthccSystemService,
    private dialogService: DialogService,
    private fb: FormBuilder,
    private router: Router,
    private translate: TranslateService,
    private authGuardSubmenu: AuthGuardSubmenu,
    public toastr: ToastsManager, vcr: ViewContainerRef,

  ) {
    this.toastr.setRootViewContainerRef(vcr);
   }

  ngOnInit() {
    this.filterForm = new FormGroup({
      name: new FormControl(),
      systemCode: new FormControl(),
      systemName: new FormControl(),
      status: new FormControl(2),
    });
    debugger
    this.tthccSystemInfo = new PageInfo<tthccSystem[]>();
    this.searchObject = new tthccSystem()
    this.searchTTHCCSystem(this.searchObject, this.currentPage)
  }
  searchTTHCCSystem(tthccSystem: tthccSystem, page: number) {
    debugger
    this.searchObject = tthccSystem;
    this.tthccSystemService.searchTTHCCSystem(tthccSystem, page)
      .then(response => {
        debugger
        this.tthccSystemInfo = response.data;
        this.tthccSystems = this.tthccSystemInfo.content;
        console.log(this.tthccSystems)
        this.pageLength = this.tthccSystemInfo.content.length;
        this.totalElements = this.tthccSystemInfo.totalElements;
        this.totalPages = this.tthccSystemInfo.totalPages;
        if (!(this.totalPages > 0)) {
          this.currentPage = -1;
        }

        this.setCurrentPage()
      }).catch(
        error => {
          console.log("no ok");
          console.log(error);
        });
  }

  deleteOneItem(id: number) {
    this.delete(id);

  }

  private delete(entityIds: number) {
    debugger;
    this.dialogService
      .confirm('Dialog.ConfirmInfo', 'Dialog.MessageConfirm')
      .subscribe(response => {
        if (response == true) {
          this.tthccSystemService.delete(entityIds)
            .then(response => {
              let message;
              if (response.code == 200) {
                this.translate.get('Message.DeleteSuccess').subscribe((res: string) => {
                  message = res;
                });
                this.toastr.success('', message, { dismiss: 'controlled' })
                  .then((toast: Toast) => {
                    setTimeout(() => {
                      this.toastr.dismissToast(toast);
                    }, 3000);
                  });
              } else if (response.code == 400) {
                this.translate.get('Message.DeleteFail').subscribe((res: string) => {
                  message = res;
                });
                this.toastr.error('', message, { dismiss: 'controlled' })
                  .then((toast: Toast) => {
                    setTimeout(() => {
                      this.toastr.dismissToast(toast);
                    }, 3000);
                  });
              }

              this.searchTTHCCSystem(this.searchObject, this.currentPage);
            })
            .catch(error => {
              let message;
              this.translate.get('Message.DeleteFail').subscribe((res: string) => {
                message = res;
              });
              this.toastr.error('', message, { dismiss: 'controlled' })
                .then((toast: Toast) => {
                  setTimeout(() => {
                    this.toastr.dismissToast(toast);
                  }, 3000);
                });
            });
        }
      })
  }


  /**
   * @description: Manage page transfers
   * @param page: Page will move to
   */
  choosePageNumber(page) {
    debugger;
    var flag = true;
    var pageNumber;

    if (page.valueAsNumber != null) {
      if (isNaN(page.valueAsNumber)) {
        flag = false;
        page.value = this.currentPage + 1;
        // this.currentPageView = 1;
      } else {
        pageNumber = page.value - 1;
      }
    } else {
      pageNumber = page;
    }

    if (flag == true && this.currentPage > pageNumber && pageNumber < 0) {
      pageNumber = 0;
    }
    if (flag == true && this.currentPage < pageNumber && pageNumber > this.totalPages - 1) {
      pageNumber = this.totalPages - 1;
    }
    if (flag == true && !Number.isInteger(pageNumber)) {
      flag = false;
      page.value = this.currentPage + 1;
    }
    if (flag == true) {

      this.currentPage = pageNumber;
      this.searchTTHCCSystem(this.searchObject, this.currentPage);
      page.value = pageNumber + 1;
    }
  }

  // set the information of the page
  private setCurrentPage() {
    if (this.tthccSystemInfo.totalElements > 0) {
      this.currentPageView = this.tthccSystemInfo.number + 1;
    }
    var numberOfElements = this.tthccSystemInfo.numberOfElements;
    var size = this.tthccSystemInfo.size;
    this.fromElement = this.tthccSystemInfo.number * size + 1;
    this.toElement = this.tthccSystemInfo.number * size + numberOfElements;
    if (this.toElement < 1) {
      this.fromElement = 0;
      this.toElement = 0;
    }
  }


  routerLinkUpdate(type) {
    debugger
    if (type == 'create') {
      debugger
      this.router.navigate(['/tthcc/create']);
    } else {
      this.tthccSystemInfo.content.forEach(item => {
        if (item.checked == true) {
          debugger
          this.router.navigate(['/tthcc/' + type, item.id]);
        }
      });
    }
  }

  /**
   * @description: export list tthccSystem to excel
   */
  // To Authorize User
  isAuthoriziedNavigation(): boolean {
    var isAuthorizied = this.authGuardSubmenu.isAuthoriziedWithCurrentUrl(this.router.url);
    return isAuthorizied;
  }


  success(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.success('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

  notice(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.warning('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

  error(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.error('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 1000);
      });
  }

}
