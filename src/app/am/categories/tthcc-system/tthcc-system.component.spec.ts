import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TthccSystemComponent } from './tthcc-system.component';

describe('TthccSystemComponent', () => {
  let component: TthccSystemComponent;
  let fixture: ComponentFixture<TthccSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TthccSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TthccSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
