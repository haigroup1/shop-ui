import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Location, DatePipe } from '@angular/common';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Toast } from 'ng2-toastr';
import { TranslateService } from '@ngx-translate/core';
import { browser } from 'protractor';
import { Constants } from '../../../common/util/constants';
import { Result } from '../../../common/util/result';
import { tthccSystemService } from '../tthcc-system.service';
import { tthccSystem } from '../tthcc-system';
import { TTHCCSystemForm } from '../tthcc-system-form.component';

@Component({
  selector: 'app-tthcc-business',
  templateUrl: './tthcc-business.component.html',
  providers: [tthccSystemService, DatePipe]
})

/**
 * @description: Component management create, update
 */
export class TTHCCSystemBusinessComponent implements OnInit {
  private sub: any
  /**the id of object */
  id: number
  /** the name of business */
  business: string
  /** the form object */
  tthccSystemForm: FormGroup
  tthccSystem: tthccSystem
  filterForm: FormGroup
  isUpdate: number
  listStatus = Constants.STATUS_LIST
  listtthccSystem: tthccSystem[]
  provinceSelections: Array<any> = []
  indexProvinceSelection: number
  listStatusSystem = Constants.SYSTEM_STATUS;
  districtSelections: Array<any> = []
  indexDistrictSelection: number
  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private tthccSystemService: tthccSystemService,
    private fb: FormBuilder,
    private translate: TranslateService,
    public toastr: ToastsManager,
    vcr: ViewContainerRef,
    private datepipe: DatePipe
  ) {
    this.toastr.setRootViewContainerRef(vcr);
  }

  ngOnInit() {
    this.filterForm = new FormGroup({
      name: new FormControl(),
      systemCode: new FormControl(),
      systemName: new FormControl(),
      status: new FormControl(),
      centerCode: new FormControl(),
      code: new FormControl(),
      description: new FormControl(),
      systemId: new FormControl()
      // name: new FormControl(),
    });

    // Lấy bản ghi theo 'id' từ @PathParam
    this.sub = this.route.params.subscribe(params => {
      debugger
      this.id = params['id'];
      this.business = params['business'];
      this.tthccSystemForm = TTHCCSystemForm.tthccSystemForm(this.fb, this.business);
      if (this.business == 'create') {
        this.isUpdate = 1;
      }
      if (this.business == 'update') {
        this.isUpdate = 2
        this.bindingData(this.tthccSystemForm, this.id)
      } if (this.business == 'createUnit') {
        this.findOne(this.id)
        // this.filterForm.get('systemId').setValue(this.id)
        console.log(this.id)
        this.isUpdate = 3;
      }
    });

  }


  parse(value: any): Date | null {
    if ((typeof value === 'string') && (value.indexOf('/') > -1)) {
      const str = value.split('/');

      const year = Number(str[2]);
      const month = Number(str[1]) - 1;
      const date = Number(str[0]);

      return new Date(year, month, date);
    } else if ((typeof value === 'string') && value === '') {
      return new Date();
    }
    const timestamp = typeof value === 'number' ? value : Date.parse(value);
    return isNaN(timestamp) ? null : new Date(timestamp);
  }


  bindingData(tthccSystemForm, id) {
    this.tthccSystemService.findOne(id)
      .then(response => {
        debugger
        this.tthccSystem = new tthccSystem()
        this.tthccSystem = response.data
        tthccSystemForm.bindingData(tthccSystemForm, this.tthccSystem)
      })
      .catch(error => console.log("errors: " + error));
  }

  findOne(id: number) {
    this.tthccSystemService.findOne(id)
      .then(response => {
        this.tthccSystem = response.data
        console.log(this.tthccSystem)
      }).catch(
        error => {
          console.log(error);
        });
  }

  /**
   * @description : submit data
   * @param ward : the data
   */
  submit(tthccSystem) {
    debugger
    if (this.isUpdate == 2) {
      this.updateWard(tthccSystem)
    } else if (this.isUpdate == 1) {
      this.createWard(tthccSystem)
    } else {
      this.createUnit(tthccSystem)
    }
  }

  isCodeExisted(response: any) {
    debugger
    if (response && response.code && response.message && response.code == 400
      && response.message == Constants.MSG_CAUTION + Result.CODE_IS_EXISTED) {
      return true
    }
    return false
  }

  /**
   * Creat a new object
   */
  private createWard(wardRef) {
    debugger
    console.log(JSON.stringify(wardRef))
    this.tthccSystemService.create(wardRef)
      .then(response => {
        let message;
        if (response.code == 200) {
          this.translate.get('Thêm mới thành công').subscribe((res: string) => {
            message = res;
          });
          this.toastr.success('', message, { dismiss: 'controlled' })
            .then((toast: Toast) => {
              setTimeout(() => {
                this.toastr.dismissToast(toast);
              }, 3000);
            });
        } else if (response.code == 400) {
          this.translate.get('Thêm mới thất bại').subscribe((res: string) => {
            message = res;
          });
          this.toastr.error('', message, { dismiss: 'controlled' })
            .then((toast: Toast) => {
              setTimeout(() => {
                this.toastr.dismissToast(toast);
              }, 3000);
            });
        }
      })
      .catch(error => {
        let message;
        this.translate.get("Thêm mới thất bại").subscribe((res: string) => {
          message = res;
        });
        this.toastr.success('', message, { dismiss: 'controlled' })
          .then((toast: Toast) => {
            setTimeout(() => {
              this.toastr.dismissToast(toast);
            }, 3000);
          });

      });
    this.goBack()
  }

  private createUnit(wardRef) {
    debugger
    console.log(JSON.stringify(wardRef))
    this.tthccSystemService.createUnit(wardRef)
      .then(response => {
        let message;
        if (response.code == 200) {
          this.translate.get('Thêm mới thành công').subscribe((res: string) => {
            message = res;
          });
          this.toastr.success('', message, { dismiss: 'controlled' })
            .then((toast: Toast) => {
              setTimeout(() => {
                this.toastr.dismissToast(toast);
              }, 3000);
            });
        } else if (response.code == 400) {
          this.translate.get('Thêm mới thất bại').subscribe((res: string) => {
            message = res;
          });
          this.toastr.error('', message, { dismiss: 'controlled' })
            .then((toast: Toast) => {
              setTimeout(() => {
                this.toastr.dismissToast(toast);
              }, 3000);
            });
        }
      })
      .catch(error => {
        let message;
        this.translate.get("Thêm mới thất bại").subscribe((res: string) => {
          message = res;
        });
        this.toastr.success('', message, { dismiss: 'controlled' })
          .then((toast: Toast) => {
            setTimeout(() => {
              this.toastr.dismissToast(toast);
            }, 3000);
          });

      });
    this.goBack()
  }

  /**
   * Update a object
   * @param ward 
   */
  private updateWard(tthccSystem) {
    debugger
    this.tthccSystemService.update(tthccSystem)
      .then(response => {
        let message;
        if (response.code == 200) {
          this.translate.get('Cập nhật thành công').subscribe((res: string) => {
            message = res;
          });
          this.toastr.success('', message, { dismiss: 'controlled' })
            .then((toast: Toast) => {
              setTimeout(() => {
                this.toastr.dismissToast(toast);
              }, 3000);
            });
        } else if (response.code == 400) {
          this.translate.get('Cấp nhật thất bại').subscribe((res: string) => {
            message = res;
          });
          this.toastr.error('', message, { dismiss: 'controlled' })
            .then((toast: Toast) => {
              setTimeout(() => {
                this.toastr.dismissToast(toast);
              }, 3000);
            });
        }

      })
      .catch(error => {
        let message;
        this.translate.get("Cập nhật thất bại").subscribe((res: string) => {
          message = res;
        });
        this.toastr.success('', message, { dismiss: 'controlled' })
          .then((toast: Toast) => {
            setTimeout(() => {
              this.toastr.dismissToast(toast);
            }, 3000);
          });

      });
    this.goBack()

  }

  /**
   * Check data is valid
   */
  isValidForm() {
    var flag = true;
    if (this.tthccSystemForm.get('systemCode').invalid) {
      if (this.tthccSystemForm.get('systemCode').errors.required || this.tthccSystemForm.get("systemCode").errors.maxlength) {
        return false;
      }
      if (this.isUpdate === 1 && this.tthccSystemForm.get('systemCode').errors.pattern != null) {
        return false;
      }
    }
    // if (this.tthccSystemForm.get('centerCode').invalid) {
    //   if (this.tthccSystemForm.get('centerCode').errors.required) {
    //     return false;
    //   }
    //   if (this.tthccSystemForm.get('centerCode').errors.pattern != null) {
    //     return false;
    //   }
    //   if (this.tthccSystemForm.get('centerCode').errors.maxlength != null) {
    //     return false;
    //   }
    // }
    // if (this.tthccSystemForm.get('code').invalid) {
    //   if (this.tthccSystemForm.get('code').errors.required) {
    //     return false;
    //   }
    //   if (this.tthccSystemForm.get('code').errors.pattern != null) {
    //     return false;
    //   }
    // }
    if (this.tthccSystemForm.get('systemName').invalid) {
      if (this.tthccSystemForm.get('systemName').errors.required) {
        return false;
      }
      if (this.tthccSystemForm.get('systemName').errors.pattern != null) {
        return false;
      }
    }
    // const isEmail = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/i;
    // if (!isEmail.test(this.tthccSystemForm.get('mail').value)) {
    //   return false
    // }
    // if(this.tthccSystemForm.get("mobile").invalid) {
    //   if (this.tthccSystemForm.get("mobile").errors.pattern) {
    //     console.log("mobile");
    //     return false
    //   }
    // }

    // if(this.tthccSystemForm.get("fax").invalid) {
    //   if (this.tthccSystemForm.get("fax").errors.maxlength != null) {
    //     return false
    //   }
    // }
    return true;
  }

  success(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.success('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 3000);
      });
  }

  notice(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.warning('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 3000);
      });
  }

  error(translate: string) {
    let message;
    this.translate.get(translate).subscribe((res: string) => {
      message = res;
    });
    this.toastr.error('', message, { dismiss: 'controlled' })
      .then((toast: Toast) => {
        setTimeout(() => {
          this.toastr.dismissToast(toast);
        }, 3000);
      });
  }

  /** 
   * return to the previous page
  */
  goBack() {
    this.location.back();
  }
}
