export class tthccSystem {

    id: number
    systemCode: string
    systemName: string
    status: number = 2
    checked: boolean

}