import { Component, OnInit, ViewContainerRef, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { PageInfo } from '../../../common/util/page-info';
import {Headers, Http} from '@angular/http';
import {HeaderField} from '../../../common/util/header-field';
import {HeaderValue} from '../../../common/util/header-value';
import {Constants} from '../../../common/util/constants';
import {DialogEmptyService} from '../../../common/dialog-empty/dialog-empty.service';
import {type} from 'os';
import { tthccSystem } from '../tthcc-system';
import { tthccSystemService } from '../tthcc-system.service';
import { tthccSystemUnitDto } from '../dto/tthcc-system-unit-dto';
import { unit } from '../../unit/unit';
import { Toast, ToastsManager } from 'ng2-toastr';
import { DialogService } from '../../../common/dialog/dialog.service';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-tthcc-system-detail',
  templateUrl: './tthcc-system-detail.component.html',
  providers: [tthccSystemService, DialogEmptyService]
})

/**
 * @description: Component management show detail
 */
export class TTHCCSystemDetailComponent implements OnInit {
  private sub: any;
  id: number;
  currentPage = 0
  // responseInfo: PageInfo<tthccSystem[]>
  edocId: number
  tthccSystem: tthccSystemUnitDto
  unit: tthccSystem[];
  searchObject: tthccSystemUnitDto
  checkFilePath : boolean;
  responseInfo: PageInfo<tthccSystemUnitDto[]>
  responses: tthccSystemUnitDto[]
  toElement: number
  currentPageView: number = 0
  fromElement: number
  // total page
  totalPages: number
  // page sizw
  pageLength: number
  // toal elements
  totalElements: number
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private dialogService: DialogService,
    public toastr: ToastsManager,
    private dialogEmptyService: DialogEmptyService,
    private translate: TranslateService,
    private responseService: tthccSystemService,
    public vcr: ViewContainerRef,
    private http: Http,
  ) { }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.responseService.findOne(this.id)
        .then(response => {
          this.tthccSystem = response.data;
          console.log(response)
          this.searchUnit(this.tthccSystem, this.currentPage)
        })
        .catch(error => {
          console.log("errors: " + error);
        })
    });
  }
  

/**
   * @description: Manage page transfers
   * @param page: Page will move to
   */
  choosePageNumber(page) {
    debugger;
    var flag = true;
    var pageNumber;

    if (page.valueAsNumber != null) {
      if (isNaN(page.valueAsNumber)) {
        flag = false;
        page.value = this.currentPage + 1;
        // this.currentPageView = 1;
      } else {
        pageNumber = page.value - 1;
      }
    } else {
      pageNumber = page;
    }

    if (flag == true && this.currentPage > pageNumber && pageNumber < 0) {
      pageNumber = 0;
    }
    if (flag == true && this.currentPage < pageNumber && pageNumber > this.totalPages - 1) {
      pageNumber = this.totalPages - 1;
    }
    if (flag == true && !Number.isInteger(pageNumber)) {
      flag = false;
      page.value = this.currentPage + 1;
    }
    if (flag == true) {

      this.currentPage = pageNumber;
      this.searchUnit(this.searchObject, this.currentPage);
      page.value = pageNumber + 1;
    }
  }

    // set the information of the page
    private setCurrentPage() {
      if (this.responseInfo.totalElements > 0) {
        this.currentPageView = this.responseInfo.number + 1;
      }
      var numberOfElements = this.responseInfo.numberOfElements;
      var size = this.responseInfo.size;
      this.fromElement = this.responseInfo.number * size + 1;
      this.toElement = this.responseInfo.number * size + numberOfElements;
      if (this.toElement < 1) {
        this.fromElement = 0;
        this.toElement = 0;
      }
    }

  goBack() {
    this.location.back();
  }


  searchUnit(response: tthccSystemUnitDto, page: number) {
    // debugger
    // this.searchObject = response;
    this.responseService.searchUnit(response, page)
      .then(response => {
        this.responseInfo = response.data;
        this.responses = this.responseInfo.content;
        console.log(this.responses)
        this.pageLength = this.responseInfo.content.length;
        this.totalElements = this.responseInfo.totalElements;
        this.totalPages = this.responseInfo.totalPages;
        this.setCurrentPage()
      }).catch(
        error => {
          console.log(error);
        });
  }

  deleteOneItem(unit: number, system: number) {
    this.delete(unit, system);
  }

  private delete(unit: number, system: number) {
    // ;
    this.dialogService
      .confirm('Dialog.ConfirmInfo', 'Dialog.MessageConfirm')
      .subscribe(response => {
        if (response == true) {
          this.responseService.deleteUnit(unit)
            .then(response => {
              let message;
              if (response.code == 200) {
                this.translate.get('Message.DeleteSuccess').subscribe((res: string) => {
                  message = res;
                });
                this.toastr.success('', message, { dismiss: 'controlled' })
                  .then((toast: Toast) => {
                    setTimeout(() => {
                      this.toastr.dismissToast(toast);
                    }, 3000);
                  });
              } else if (response.code == 400) {
                this.translate.get("Xóa thất bại").subscribe((res: string) => {
                  message = res;
                });
                this.toastr.error('', message, { dismiss: 'controlled' })
                  .then((toast: Toast) => {
                    setTimeout(() => {
                      this.toastr.dismissToast(toast);
                    }, 3000);
                  });
              }

              this.searchUnit(this.searchObject, this.currentPage);
            })
            .catch(error => {
              let message;
              this.translate.get("Xóa thất bại").subscribe((res: string) => {
                message = res;
              });
              this.toastr.success('', message, { dismiss: 'controlled' })
                .then((toast: Toast) => {
                  setTimeout(() => {
                    this.toastr.dismissToast(toast);
                  }, 3000);
                });
            });
        }
      })
  }

  routerLinkUpdate(type) {
    debugger
    if (type == 'createUnit') {
      debugger
      this.router.navigate(['/tthcc/createUnit']);
    } else {
      // this.tthccSystemInfo.content.forEach(item => {
      //   if (item.checked == true) {
      //     debugger
      //     this.router.navigate(['/tthcc/' + type, item.id]);
      //   }
      // });
    }
  }

}
