import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Constants } from '../../common/util/constants';
import { tthccSystem } from './tthcc-system';

/**
 * @description: Define the province form. Use for enter data of the province object
 */
export class TTHCCSystemForm {
    /**
     * @description Define the province form
     * @param fb 
     */
    static tthccSystemForm(fb: FormBuilder, business: string): FormGroup {
        var tthccSystemForm: FormGroup;

        tthccSystemForm = fb.group({
            id: [],
            systemCode: [],
            systemName: [],
            status: [],
            code: [],
            description: [],
            centerCode:[],
            name: [],
            systemId: []
        });
        return tthccSystemForm;
    }

    static bindingData(tthccSystemForm: FormGroup, agency: tthccSystem) {
        
      tthccSystemForm.patchValue({
            id: agency.id,
            systemCode: agency.systemCode,
            systemName: agency.systemName,
            status: agency.status
        });
        
    }


}
