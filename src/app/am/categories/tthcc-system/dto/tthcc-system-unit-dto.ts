export class tthccSystemUnitDto {
    centerCode: string
    code: string
    description: string
    name: string
    systemId: number
    unitId: number
    systemCode: string
    systemName: string
    status: number
    checked: boolean
    id: number
}