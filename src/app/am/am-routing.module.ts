import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AmComponent } from './am.component';
import { AuthGuard } from '../authentication/guard/auth.guard';
const routes: Routes = [
  {
    path: '', component: AmComponent,
    canActivate: [AuthGuard],
    children: [
      // home application
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', loadChildren: './common/shared/home/home.module#HomeModule' },
      { path: 'user-profile', loadChildren: './common/shared/user-info/user-info.module#UserInfoModule' },
      { path: 'logRequest', loadChildren: './categories/log-request/log-request.module#LogRequestModule' },
      { path: 'logResponse', loadChildren: './categories/log-response/log-response.module#LogResponseModule' },
      { path: 'exchange-messages', loadChildren: './categories/exchange-messages/exchange-messages.module#ExchangeMessagesModule' },
      { path: 'logCompareReq', loadChildren: './categories/log-compare-resquest/log-compare-resquest.module#LogCompareResquestModule' },
      { path: 'logCompareRes', loadChildren: './categories/log-compare-response/log-compare-response.module#LogCompareResponseModule' },
      { path: 'hscv', loadChildren: './categories/hscv-system/hscv-system.module#HSCVSystemModule' },

    ]
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AmRoutingModule { }
