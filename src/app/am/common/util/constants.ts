export class Constants {
    // the status of object
    static STATUS_LIST = [
        { id: 1, name: 'Đang sử dụng' },
        { id: 0, name: 'Không sử dụng' } 
    ]

    static DATE_FROM_TO = 7;

    static STATUS_LIST_SYN = [
        { id: 1, name: 'Thành công' },
        { id: 2, name: 'Thất bại' } 
    ]

    static SYSTEM_STATUS = [
        { id: 1, name: 'Hoạt động' },
        { id: 0, name: 'Không hoạt động' } 
    ]

    static TABLE_NAME = [
        { id: 'syn_status', name: 'Đồng bộ văn bản Status' },
        { id: 'syn_edoc', name: 'Đồng bộ văn bản Edoc' }
    ]

    static TABLE_NAME_AGENCY = [
        { id: 'syn_agency', name: 'Đồng bộ đơn vị liên thông' }
    ]

    static BUSINESS_LIST = [
        { id: '0', name: 'Văn bản điện tử mới' },
        { id: '1', name: 'Thu hồi văn bản điện tử' },
        { id: '2', name: 'Cập nhật văn bản điện tử' },
        { id: '3', name: 'Văn bản thay thế' },
        { id: '4', name: 'Chưa xác định' }
    ]

    static IS_SENDER = [
        {id: 1, name: 'Văn bản trạng thái gửi đi'},
        {id: 0, name: 'Văn bản trạng trái gửi đến'}
    ]

    static API_NAME = [
        { id: '1', name: 'Lấy danh sách đơn vị liên thông' },
        { id: '2', name: 'Đăng ký/cập nhật đơn vị liên thông' },
        { id: '3', name: 'Hủy đăng ký đơn vị liên thông' },
        { id: '4', name: 'Gửi văn bản đi' },
        { id: '5', name: 'Lấy danh sách văn bản đến' },
        { id: '6', name: 'Nhận thông tin 1 văn bản' },
        { id: '7', name: 'Đổi trạng thái văn bản' },
        { id: '8', name: 'Lấy trạng thái văn bản' },
        { id: '10', name: 'Đồng bộ văn bản trạng thái' },
    ]

    static MEDTHOD_LIST = [
        { id: 'GET', name: 'GET' },
        { id: 'POST', name: 'POST' },
        { id: 'PUT', name: 'PUT' },
        { id: 'DELETE', name: 'DELETE' },
    ]

    static STATUS_LIST_RESPONSE = [
        { id: 0, name: 'Thất bại' },
        { id: 1, name: 'Thành công' }
    ]

    static STATUS_CODE_LIST = [
        { id: '01', name: 'Đã đến' },
        { id: '02', name: 'Từ chối yêu cầu' } ,
        { id: '03', name: 'Đã tiếp nhận' },
        { id: '04', name: 'Phân công' },
        { id: '05', name: 'Đang xử lý' },
        { id: '06', name: 'Hoàn thành' },
        { id: '13', name: 'Lấy lại' },
        { id: '15', name: 'Đồng ý lấy lại/cập nhật văn bản' },
        { id: '16', name: 'Từ chối lấy lại/cập nhật văn bản' },
    ]

    static SYNCHRONIZE_TIME = [
        { id: 1, name: '1 phút'},
        { id: 2, name: '2 phút'},
        { id: 15, name: '15 phút' },
        { id: 30, name: '30 phút' } ,
        { id: 60, name: '60 phút' },
        { id: 120, name: '120 phút' }
    ]

    static SYNCHRONIZEAGENCY_TIME = [
        { id: 1440, name: '1 ngày' },
        { id: 2880, name: '2 ngày' } ,
        { id: 7200, name: '5 ngày' },
        { id: 14400, name: '10 ngày' }
    ]

    static INDEPENDENCE_LIST = [
        { id: 'Độc lập', name: 'Độc lập' },
        { id: 'Chưa độc lập', name: 'Chưa độc lập' } 
    ]

    // the contact of object
    static CONTACT_LIST = [
        { id: 1, name: 'TTHC liên thông' },
        { id: 2, name: 'TTHC không liên thông' } 
    ]

    // the type of object
    static TYPE_LIST = [
        { id: 1, name: 'TTHC có thuế' },
        { id: 2, name: 'TTHC không thuế' } 
    ]

    // the type of object
    static TYPEPUBLICSERVICE_LIST = [
        { id: 1, name: 'Cấp 1' },
        { id: 2, name: 'Cấp 2' },
        { id: 3, name: 'Cấp 3' },
        { id: 4, name: 'Cấp 4' }
    ]

    static ACCESS_KEY =
        {
            Add: 1,
            Edit: 2,
            Delete: 0,
            Import: 3

        }

    static PAGE_SIZE = 10;
    static PAGE_SIZE2 = 5;
    static PAGE_SIZE1 = 2000;

    static NAME_PATTERN = "(( *)[a-zA-Z\*%^!@#$\:;&=\(\)'\\-`.+,/\"0-9_-ẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹý]( *)+)*$";
// [^a-z]*[a-z]*[^a-z]
    static CODE_PATTERN = "^( *)[^a-zA-Z\*%^!@#$\:;&=\(\)'\\-`.+,/\"0-9_-ẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹý]*[a-zA-Z\*%^!@#$\:;&=\(\)'\\-`.+,/\"0-9_-ẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹý]*[^a-zA-Z\*%^!@#$\:;&=\(\)'\\-`.+,/\"0-9_-ẮẰẲẴẶĂẤẦẨẪẬÂÁÀÃẢẠĐẾỀỂỄỆÊÉÈẺẼẸÍÌỈĨỊỐỒỔỖỘÔỚỜỞỠỢƠÓÒÕỎỌỨỪỬỮỰƯÚÙỦŨỤÝỲỶỸỴàáãạảăắằẳẵặâấầẩẫậèéẹẻẽêềếểễệđìíĩỉịòóõọỏôốồổỗộơớờởỡợùúũụủưứừửữựỳỵỷỹý]*$";

    static IP_PATTERN = "^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$";

    static USERNAME_PATTERN = "^[a-z0-9_-]{3,15}$";

    static PASSWORD_PATTERN = "((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";

    static EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    static IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";

    static NUMBER_PATTERN = "^(0|[1-9][0-9.]*)$"

    static DOUBLE_PATTERN = "^(0|[0-9.]*)$"

    static PHONE_NUMBER_PATTERN = "\\d{10}|(?:\\d{3}-){2}\\d{4}|\\(\\d{3}\\)\\d{3}-?\\d{4}";

    static SSO_CLIENT_ID = "YzmuxTn3HRo4YGwLuaFKxIFmRPQa";

    static SSO_CALLBACK_URL = "http://192.168.10.142:9094/processing/";

    static SSO_IS_AUTH_URL = "https://sso.savis.vn";

    static SSO_IS_AUTH_INFO_URL = "http://sso.savis.vn:9763";

    static SSO_LOGOUT_URL = "https://sso.savis.vn/commonauth?commonAuthLogout=true&type=oidc&commonAuthCallerPath=http://192.168.10.142:9094";

    static AUTHORIZATION_CODE = "authorizationCode";
    static IS_AUTHENTIC = "isAuthentic";
    static CURRENT_USER = "currentUser";
    static ACCESS_TOKEN = "accessToken";
    static REFRESH_TOKEN = "refreshToken";
    static EXPIRE_TIME = "expireYime";
    static SESSION_STATE = "sessionState";
    static KEY_LANGUAGE = "keyLanguage";
    static RETURN_URL = "returnUrl";

    /* Variables export excel*/
    static EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
    static EXCEL_EXTENSION = '.xlsx';

    /* Names of file export excel */
    static COUNTRY_EXCEL = "Country_Excel";
    static CURRENCY_EXCEL = "Currency_Excel";
    static DEPARTMENT_EXCEL = "Department_Excel";
    static DISTRICT_EXCEL = "District_Excel";
    static DMDVKIEMTOAN_EXCEL = "Dmdvkiemtoan_Excel"
    static LINHVUCKT_EXCEL = "Linhvuckt_Excel"
    static NATION_EXCEL = "Nation_Excel";
    static ORGANIZATION_EXCEL = "Organization_Excel"
    static POSITION_EXCEL = "Position_Excel"
    static PROVICNE_EXCEL = "Province_Excel";
    static RELIGION_EXCEL = "Religion_Excel";
    static REPORTFORM_EXCEL = "ReportForm_Excel";
    static REPORTTARGET_EXCEL = "ReportTarget_Excel";
    static WARD_EXCEL = "Ward_Excel";
    
    static MSG_CAUTION = "Chú ý: "
}