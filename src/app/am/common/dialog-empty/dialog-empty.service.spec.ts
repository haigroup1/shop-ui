import { TestBed, inject } from '@angular/core/testing';

import { DialogEmptyService } from './dialog-empty.service';

describe('DialogEmptyService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DialogEmptyService]
    });
  });

  it('should be created', inject([DialogEmptyService], (service: DialogEmptyService) => {
    expect(service).toBeTruthy();
  }));
});
