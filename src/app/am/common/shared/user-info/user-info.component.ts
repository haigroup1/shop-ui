import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/src/toast-manager';
import { UserInfo } from '../../../../authentication/sso-processing/user-info';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Constants } from '../../util/constants';
import { UserForm } from './user-form.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css'],
  providers: [ToastsManager]
})
export class UserInfoComponent implements OnInit {

  userInfo: UserInfo;
  updateInfoForm: FormGroup;
  changePasswordForm: FormGroup;

  constructor(
    public fb: FormBuilder,
    private translate: TranslateService,
    private router: Router,
    public toastr: ToastsManager, vcr: ViewContainerRef,

  ) { }

  ngOnInit() {
    debugger;
    this.userInfo = JSON.parse(localStorage.getItem(Constants.CURRENT_USER));
    this.updateInfoForm = UserForm.updateInfo(this.fb);
    this.changePasswordForm = UserForm.changePasswordForm(this.fb);
    UserForm.bindingDataInfo(this.updateInfoForm, this.userInfo);
  }

  Cancel() {
    this.router.navigate(['/home']);
  }
}
