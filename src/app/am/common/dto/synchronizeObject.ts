export class SynchronizeObject{
    id: number;
    tableName: string = "";
    lastSuccess: Date;
    status: string = "";
    checked: boolean
    startDate: Date = new Date(Date.now() - 30 * 24 * 60 * 60 * 1000)
    endDate: Date = new Date
}